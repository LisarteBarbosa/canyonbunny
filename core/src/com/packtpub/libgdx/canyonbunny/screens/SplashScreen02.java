/*******************************************************************************
 * Lisarte Barbosa
 ******************************************************************************/

package com.packtpub.libgdx.canyonbunny.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
//import com.packtpub.libgdx.canyonbunny.app.Assets;
import com.packtpub.libgdx.canyonbunny.screens.transitions.ScreenTransition;
import com.packtpub.libgdx.canyonbunny.screens.transitions.ScreenTransitionFade;
//import com.packtpub.libgdx.canyonbunny.screens.transitions.ScreenTransitionSlide;
import com.packtpub.libgdx.canyonbunny.util.Constants;

public class SplashScreen02 extends AbstractGameScreen {

    private static final String TAG = SplashScreen02.class.getName();

    private Stage stage;
    Texture splashTexture;

    Sprite splashSprite;

    float screenTimingLeft;

    boolean inTransition = false;

    private Image imgBackground;
    float rgb = 0f;

    // debug
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private boolean debugEnabled = false;
    private float debugRebuildStage;

    public SplashScreen02(DirectedGame app) {
        super(app);
    }

    @Override
    public void render(float deltaTime) {

        checkNextScreen(deltaTime);

        Gdx.gl.glClearColor(255 / 255.0f, 102 / 255.0f, 0 / 255.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (debugEnabled) {
            debugRebuildStage -= deltaTime;
            if (debugRebuildStage <= 0) {
                debugRebuildStage = DEBUG_REBUILD_INTERVAL;
                rebuildStage();
            }
        }
        stage.act(deltaTime);
        stage.draw();
        //		Table.drawDebug(stage);

    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
    }

    @Override
    public void show() {
        stage = new Stage();

        splashTexture = new Texture("images/LogoFullOrange768h.png");
        splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        screenTimingLeft = Constants.SPLASH_SCREEN_02_TOTAL_DURATION;

        //Assets.instance.init(new AssetManager());
        rebuildStage();

        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void hide() {

        Gdx.input.setCatchBackKey(false);

        splashTexture.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {
        //nothing to save in case of pause status change
    }

    private void rebuildStage() {

        // build all layers
        Table layerBackground = buildBackgroundLayer();

        // assemble stage for menu screen
        stage.clear();
        Stack stack = new Stack();
        stage.addActor(stack);
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.add(layerBackground);
    }

    private void checkNextScreen(float deltaTime) {
        screenTimingLeft -= deltaTime;
        //if (Assets.loaded && !inTransition && screenTimingLeft < 0) {
        if (!inTransition && screenTimingLeft < 0) {
            inTransition = true;

            //call next screen
            ScreenTransition transition = ScreenTransitionFade.init(2.0f);
            game.setScreen(new MenuScreen(game), transition);
        }
    }

    private Table buildBackgroundLayer() {
        Table layer = new Table();

        TextureRegion splashRegion = new TextureRegion(splashTexture, 0, 0, 768, 384);

        imgBackground = new Image(splashRegion);

        layer.add(imgBackground);
        return layer;
    }

    @Override
    public InputProcessor getInputProcessor() {
        return stage;
    }

}
