/*******************************************************************************
 * Lisarte Barbosa
 * 
 ******************************************************************************/

package com.packtpub.libgdx.canyonbunny.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.packtpub.libgdx.canyonbunny.screens.transitions.ScreenTransition;
import com.packtpub.libgdx.canyonbunny.screens.transitions.ScreenTransitionSlice;
import com.packtpub.libgdx.canyonbunny.util.Constants;

/**
 * @author ma5ter First SplashScreen
 */
public class SplashScreen01 extends AbstractGameScreen {

    private static final String TAG = SplashScreen01.class.getName();

    private Stage stage;
    Texture splashTexture;

    Sprite splashSprite;
    boolean inTransition;
    float screenTimingLeft;

    private Image imgBackground;

    // debug
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private boolean debugEnabled = false;
    private float debugRebuildStage;

    public SplashScreen01(DirectedGame app) {
        super(app);

    }

    /*
     * cycled function for each graphic frame renders the SplashScreen depending in the deltatime
     * @param deltatime - graphics timer counter
     */
    @Override
    public void render(float deltaTime) {

        checkNextScreen(deltaTime);

        //clear screen with color GREY
        Gdx.gl.glClearColor(126 / 255.0f, 126 / 255.0f, 126 / 255.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (debugEnabled) {
            debugRebuildStage -= deltaTime;
            if (debugRebuildStage <= 0) {
                debugRebuildStage = DEBUG_REBUILD_INTERVAL;
                rebuildStage();
            }
        }
        stage.act(deltaTime);
        stage.draw();
        //		Table.drawDebug(stage);

    }

    /*
     * Change the internal Viewport in case of a screen size alteration
     */
    @Override
    public void resize(int width, int height) {
        stage.setViewport(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
    }

    /*
     * First initialization of this class, first status initializer
     */
    @Override
    public void show() {
        stage = new Stage();

        try {
            splashTexture = new Texture("images/LogoSingleGrey768h.png");
            splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        } catch (Exception e) {
            //graphic Splash Screen: SomeOne Tempered the Game files system
        }
        screenTimingLeft = Constants.SPLASH_SCREEN_01_TOTAL_DURATION;
        rebuildStage();
    }

    @Override
    public void hide() {

        splashTexture.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {
    }

    /**
     * Rebuild Stage preparing elements and gathering them together
     */
    private void rebuildStage() {

        // build all layers
        Table layerBackground = buildBackgroundLayer();

        // assemble stage for menu screen
        stage.clear();
        Stack stack = new Stack();
        stage.addActor(stack);
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.add(layerBackground);
    }

    //check if it is time for next screen
    private void checkNextScreen(float deltaTime) {
        screenTimingLeft -= deltaTime;
        if (!inTransition && screenTimingLeft < 0) {
            inTransition = true;
            //call next screen
            ScreenTransition transition = ScreenTransitionSlice.init(Constants.SPLASH_SCREEN_02_ANIM_DURATION, ScreenTransitionSlice.UP_DOWN, 20, Interpolation.pow5Out);
            game.setScreen(new SplashScreen02(game), transition);
        }
    }

    /**
     * Aggregates objects and actors for background layer
     * 
     * @return layer - Table layer with the elements
     */
    private Table buildBackgroundLayer() {
        Table layer = new Table();

        TextureRegion splashRegion = new TextureRegion(splashTexture, 0, 0, 768, 384);

        imgBackground = new Image(splashRegion);

        layer.add(imgBackground);
        return layer;
    }

    @Override
    public InputProcessor getInputProcessor() {
        return stage;
    }

}
